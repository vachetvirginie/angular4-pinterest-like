import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {
  id:number
   description:string 
   link:string
   owner:number
   likes:number[] 
  constructor() { }

  ngOnInit() {
  }

}
