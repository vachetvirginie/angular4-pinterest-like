import { Injectable } from '@angular/core';
import { User } from './class/users';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class UsersService {
private urlAPI:string='http://localhost3000/users';
  constructor(private http:HttpClient) { }
  getUser(){
    return this.http.get<User[]>(this.urlAPI);
  }
getUserById(id:number){
return this.http.get<User[]>(this.urlAPI+'/'+id);
}
addUser(){

}
removeUser(){

}
updateuser(){

}
}
